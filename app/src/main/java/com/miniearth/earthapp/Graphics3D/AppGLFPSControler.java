package com.miniearth.earthapp.Graphics3D;

import android.content.Context;
import android.widget.TextView;

import com.miniearth.earthapp.AndroidUI.MainActivity;
import com.miniearth.earthapp.R;

/**
 * Created by kath on 6/1/2015.
 */
public class AppGLFPSControler {

    protected long startTime = 0;
    public void initFPSControl(){
        startTime = System.currentTimeMillis();
    }

    public void addFPSControl(final Context context){
        long endTime = System.currentTimeMillis();
        final long dt = endTime - startTime;

        ((MainActivity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView)((MainActivity)context).findViewById(R.id.fps_monitor)).setText(String.format( "FPS:  %.2f",  (double)dt));

            }
        });

        if (dt < 33)
            try {
                Thread.sleep(33 - dt);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        startTime = System.currentTimeMillis();
    }
}
