package com.miniearth.earthapp.Graphics3D;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import com.miniearth.earthapp.Models3D.Earth;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by kath on 5/30/2015.
 */
public class AppGLRenderer extends AppGLFPSControler implements GLSurfaceView.Renderer {

    //private Test3DCubeModel mTest3DCubeModel;
    private Earth earth;
    private Context context;
    private float ratio;
    private Runnable m3dOperation;

    public AppGLRenderer(Context context) {
        this.context = context;
    }

    // If we call autorotate when we create the renderer
    // then we dont know if surface was created and
    // we might end up with a NPE.
    //SOLUTION: we buffer the operation
    private void load3dOperation(Runnable m3dOperation) {
        this.m3dOperation = m3dOperation;
    }

    private void consume3dOperation() {
        if (m3dOperation != null)
            m3dOperation.run();
        m3dOperation = null; //delete it
    }

    public void autoRotate() {
        load3dOperation(new Runnable() {
            @Override
            public void run() {
                earth.getEarthRotationManager().autoRotate();
            }
        });
    }

    public void freeRotate() {
        load3dOperation(new Runnable() {
            @Override
            public void run() {
                earth.getEarthRotationManager().freeRotate();
            }
        });
    }

    public void lockPosition() {
        load3dOperation(new Runnable() {
            @Override
            public void run() {
                earth.getEarthRotationManager().lockPosition();
            }
        });
    }

    public void setRotationAngleX(float rotationAngleX) {
        earth.getEarthRotationManager().setRotationAngleX(rotationAngleX);
    }

    public void setRotationAngleY(float rotationAngleY) {
        earth.getEarthRotationManager().setRotationAngleY(rotationAngleY);
    }

    public void setRotationAngleZ(float rotationAngleZ) {
        earth.getEarthRotationManager().setRotationAngleZ(rotationAngleZ);
    }

    public final void setZoomScaleFactor(float mult) {
        earth.getEarthDisplayManager().setZoomScaleFactor(mult);
    }

    public float getRotationAngleX() {
        return earth.getEarthRotationManager().getRotationAngleX();
    }

    public float getRotationAngleY() {
        return earth.getEarthRotationManager().getRotationAngleY();
    }

    public void changeTexture(Bitmap bitmap) {
        if (earth != null) {
            earth.clearGLTextures();
            loadTextureFromBitmap(bitmap);
        }
    }

    /**
     *slower call to texture because renderer has to transform resId into Bitmap than apply
     */
    public void loadTextureFromResource(Context context, int resId){
        earth.loadGLTexture(context, resId);
    }

    /**
     *faster call to texture because renderer will have an already cached bitmap into memory
     */
    public void loadTextureFromBitmap(Bitmap bitmap){
        earth.loadBitmapGLTexture(bitmap);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, javax.microedition.khronos.egl.EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        earth = new Earth(5, 1,context);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        loadTextureFromResource(context, Earth.getEarthTextureResource());
        initFPSControl();
    }

    public void onDrawFrame(GL10 unused) {
        // Reset frame and consume any pending
        // 3d operation we might have stored
        clear();
        consume3dOperation();

        // This will ensure a steady 20-30 fps so
        // that our autorotate wont fluctuate
        // based on fps.
        // RESULTS: intentional thread delaying
        addFPSControl(context);

        // All the results above are combined.
        // RESULTS: canvasMatrix
        earth.draw(ratio);
    }

    private void clear() {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT);
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        ratio = (float) width / height;
    }
}
