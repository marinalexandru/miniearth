package com.miniearth.earthapp.Graphics3D;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;
import com.miniearth.earthapp.Utils.EarthConstants;
import com.miniearth.earthapp.Utils.EarthMath;


/**
 * Created by kath on 5/30/2015.
 */
public class AppSurfaceView extends GLSurfaceView {

    private final AppGLRenderer earthRenderer;

    private boolean allowCollectUserInput = false;

    private float mPreviousX = 0;
    private float mPreviousY = 0;

    private int POINTER_TYPE = 0;
    private float mFingerDistance = 0;
    private float mPreviousSpanAngle = 0;

    private float initialTouch1X = 0;
    private float initialTouch1Y = 0;
    private float initialTouch2X = 0;
    private float initialTouch2Y = 0;

    public AppSurfaceView(Context context) {
        super(context);

        setEGLContextClientVersion(EarthConstants.SURFACE_VIEW_CONSTANTS.OPEN_GL_ES_2_0_VERSION);
        earthRenderer = new AppGLRenderer(getContext());
        setRenderer(earthRenderer);
    }


    public void changeTextureBitmap(final Bitmap bitmap){
        queueEvent(new Runnable() {
            @Override
            public void run() {
                earthRenderer.changeTexture(bitmap);
            }
        });

    }

    public void setAutoRotate() {
        allowCollectUserInput = false;
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        earthRenderer.autoRotate();
    }

    public void setFreeRotate() {
        allowCollectUserInput = true;
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        earthRenderer.freeRotate();
    }

    public void setLockIntoPosition() {
        allowCollectUserInput = false;
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        earthRenderer.lockPosition();
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchEvent) {
        if (touchEvent == null)
            return true;
        if (!allowCollectUserInput)
            return true;
        float x = touchEvent.getX();
        float y = touchEvent.getY();
        switch (touchEvent.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                mPreviousX = x;
                mPreviousY = y;
                Log.d(EarthConstants.DEBUG.TAG_GESTURES_INFO, "1 finger down");
                POINTER_TYPE = EarthConstants.SURFACE_VIEW_CONSTANTS.ONE_TOUCH_ON_SCREEN;
                return true;

            case MotionEvent.ACTION_POINTER_DOWN:
                mPreviousSpanAngle = 0;
                Log.d(EarthConstants.DEBUG.TAG_GESTURES_INFO, "2 fingers down");
                POINTER_TYPE = EarthConstants.SURFACE_VIEW_CONSTANTS.SECOND_TOUCH_ON_SCREEN;
                mFingerDistance = EarthMath.getFingerSpan(touchEvent);
                initialTouch1X = touchEvent.getX(0);
                initialTouch1Y = touchEvent.getY(0);
                initialTouch2X = touchEvent.getX(1);
                initialTouch2Y = touchEvent.getY(1);
                return true;

            case MotionEvent.ACTION_POINTER_UP:
                POINTER_TYPE = EarthConstants.SURFACE_VIEW_CONSTANTS.CONSIDER_NO_TOUCH_ON_SCREEN;
                //we have one touch but it may create bugs
                return true;

            case MotionEvent.ACTION_MOVE:
                if (POINTER_TYPE == EarthConstants.SURFACE_VIEW_CONSTANTS.ONE_TOUCH_ON_SCREEN) {
                    //rotate XY
                    float dx = x - mPreviousX;
                    float dy = y - mPreviousY;
                    earthRenderer.setRotationAngleX(earthRenderer.getRotationAngleX() + dx * EarthConstants.SURFACE_VIEW_CONSTANTS.TOUCH_SCALE_FACTOR);
                    earthRenderer.setRotationAngleY(earthRenderer.getRotationAngleY() + dy * EarthConstants.SURFACE_VIEW_CONSTANTS.TOUCH_SCALE_FACTOR);
                    requestRender();
                }
                if (POINTER_TYPE == EarthConstants.SURFACE_VIEW_CONSTANTS.SECOND_TOUCH_ON_SCREEN) {


                    //zoom
                    float newDist = EarthMath.getFingerSpan(touchEvent);
                    float d = mFingerDistance / newDist;
                    earthRenderer.setZoomScaleFactor(d);
                    mFingerDistance = newDist;
                    //rotate
                    float mCurrentRotationAngle = EarthMath.getSpanAngle(touchEvent, initialTouch1X, initialTouch1Y, initialTouch2X, initialTouch2Y);
                    earthRenderer.setRotationAngleZ(mCurrentRotationAngle - mPreviousSpanAngle);
                    mPreviousSpanAngle = mCurrentRotationAngle;
                }
                break;
        }
        mPreviousX = x;
        mPreviousY = y;
        return true;
    }
}
