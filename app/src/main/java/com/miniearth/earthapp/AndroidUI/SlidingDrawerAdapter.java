package com.miniearth.earthapp.AndroidUI;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.miniearth.earthapp.R;

import java.util.List;

/**
 * Created by kath on 6/9/2015.
 */
public class SlidingDrawerAdapter extends ArrayAdapter<DrawerItemModel> {
    private final Context context;
    public List<DrawerItemModel> items;
    public SlidingDrawerAdapter(Context context, List<DrawerItemModel> objects) {
        super(context,getRowLayout() , objects);
        this.context=context;
        this.items = objects;
    }

    public static int getRowLayout(){
        return R.layout.row_sliding;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(getRowLayout(), parent, false);
            vh = new ViewHolder();
            vh.menu_item_name = (TextView) convertView.findViewById(R.id.menu_item_name);
            vh.menu_item_image = (ImageView) convertView.findViewById(R.id.menu_item_image);
            vh.background =  convertView.findViewById(R.id.background);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        DrawerItemModel currentModel = items.get(position);
        vh.menu_item_name.setText(currentModel.getName());
        vh.menu_item_image.setImageResource(currentModel.getResourceThumbId());

        if (currentModel.getSelected()){
            vh.background.setBackgroundColor(Color.WHITE);
            vh.menu_item_name.setTextColor(Color.BLACK);
        }else{
            vh.background.setBackgroundColor(Color.BLACK);
            vh.menu_item_name.setTextColor(Color.WHITE);
        }

        return convertView;
    }

    class ViewHolder{
        TextView menu_item_name;
        ImageView menu_item_image;
        View background;
    }
}
