package com.miniearth.earthapp.AndroidUI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

import com.miniearth.earthapp.Graphics2D.HeatMapFactory;
import com.miniearth.earthapp.Graphics3D.AppSurfaceView;
import com.miniearth.earthapp.Models3D.Earth;
import com.miniearth.earthapp.WeatherLogicAndSync.TestGeographicalDataFactory;
import com.miniearth.earthapp.WeatherLogicAndSync.WeatherReading;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kath on 7/4/15.
 */
public class ChangeTextureOnSlideActivity extends ActionBarActivity {
    protected AppSurfaceView appGLView;
    private ArrayList<Bitmap> bitmapCachedList;
    private ArrayList<String> displayTimeList;
    public static int MIN_READING;
    public static int MAX_READING;

    protected void loadHeatMaps(int READING_TYPE) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        final Bitmap bitmap = BitmapFactory.decodeResource(getResources(), Earth.getEarthTextureResource(), options);
        List<WeatherReading> data = getDataAndApplyLegend(READING_TYPE, new OnLegendDataAvailable() {
            @Override
            public void onMinMaxDetermined(int minValue, int maxValue) {
                MIN_READING = minValue;
                MAX_READING = maxValue;
                displayValueOnLegend(minValue, maxValue);
            }
        });
        List<DailyReadings> dailyReadingsList = DailyReadingsFactory.getDailyReadingsList(data);
        bitmapCachedList = new ArrayList<>();
        displayTimeList = new ArrayList<>();
        for (DailyReadings dailyReadings : dailyReadingsList) {
            HeatMapFactory hmp = new HeatMapFactory(ChangeTextureOnSlideActivity.this, bitmap, READING_TYPE, dailyReadings.getWeatherReadings());
            Bitmap bmp = hmp.getMarkedBitmap();
            bitmapCachedList.add(bmp);
            displayTimeList.add(dailyReadings.getWeatherReadings().get(0).getTime());
        }
        appGLView.changeTextureBitmap(bitmapCachedList.get(0));
    }

    protected void setSeekBar(View view) {
        SeekBar seekBar = (SeekBar) view;
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.d("progres_bar", "onProgressChanged");
                onSliderMoved(seekBar);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("progres_bar", "onStartTrackingTouch");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("progres_bar", "onStopTrackingTouch");
            }
        });
        seekBar.setMax(bitmapCachedList.size() - 1);
        seekBar.setProgress(0);
        onSliderMoved(seekBar);
    }

    int oldPosition = 0;

    private void onSliderMoved(SeekBar seekBar) {
        int position = seekBar.getProgress();
        if (position == oldPosition)
            return;
        oldPosition = position;

        appGLView.changeTextureBitmap(bitmapCachedList.get(position));
        updateTimeDisplay(displayTimeList.get(position));
    }



    /**
     * Overrided from child classes like MainActivity.class
     * @param time
     */
    protected void updateTimeDisplay(String time) {

    }


    public static class DailyReadingsFactory {
        public static List<DailyReadings> getDailyReadingsList(List<WeatherReading> weatherReadings) {
            List<String> times = new ArrayList<>();
            List<DailyReadings> dailyReadingses = new ArrayList<>();
            for (WeatherReading weatherReading : weatherReadings) {
                Boolean added = false;
                for (String time : times)
                    if (weatherReading.getTime().equals(time)) {
                        dailyReadingses.get(times.indexOf(time)).getWeatherReadings().add(weatherReading);
                        added = true;
                    }
                if (added)
                    continue;
                DailyReadings newDailyReadings = new DailyReadings(new ArrayList<WeatherReading>());
                newDailyReadings.getWeatherReadings().add(weatherReading);
                dailyReadingses.add(newDailyReadings);
                times.add(weatherReading.getTime());
            }
            return dailyReadingses;
        }
    }

    /**
     * To be overrided in child classes ex: MainActivity.class
     */
    protected void displayValueOnLegend(int min, int max) {

    }

    public static class DailyReadings implements Serializable {
        List<WeatherReading> weatherReadings;

        public DailyReadings(List<WeatherReading> weatherReadings) {
            this.weatherReadings = weatherReadings;
        }

        public List<WeatherReading> getWeatherReadings() {
            return weatherReadings;
        }
    }

    private List<WeatherReading> getDataAndApplyLegend(int READING_TYPE, OnLegendDataAvailable legendDataAvailable) {

        //DAY 1
        List<WeatherReading> weatherReadings = TestGeographicalDataFactory.createData();

        WeatherReading.setReadingType(READING_TYPE);
        legendDataAvailable.onMinMaxDetermined(Collections.min(weatherReadings).getDisplayedField(), Collections.max(weatherReadings).getDisplayedField());
        for (WeatherReading weatherReading : weatherReadings) {
            weatherReading.setNormalisedDisplayReading(((float) weatherReading.getDisplayedField() - MIN_READING) /
                    ((float) MAX_READING - MIN_READING));
        }
        return weatherReadings;
    }

    public interface OnLegendDataAvailable {
        void onMinMaxDetermined(int min, int max);
    }

}
