package com.miniearth.earthapp.AndroidUI;

import com.miniearth.earthapp.R;

import java.io.Serializable;

/**
 * Created by kath on 6/9/2015.
 */
public class DrawerItemModel implements Serializable{

    String name;
    int resourceThumbId;
    int readingType;
    Boolean selected = false;

    public DrawerItemModel (String name, int resourceThumbId, int readingType){
        this.name = name;
        this.resourceThumbId = resourceThumbId;
        this.readingType = readingType;
    }

    public int getResourceThumbId() {
        return resourceThumbId;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public int getReadingType() {
        return readingType;
    }
}
