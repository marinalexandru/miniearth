package com.miniearth.earthapp.AndroidUI;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.miniearth.earthapp.R;

/**
 * Created by kath on 7/12/15.
 */
public class LauncherActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Handler postDelayedHandler = new Handler();
        postDelayedHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(LauncherActivity.this,MainActivity.class);
                startActivity(mainIntent);
                LauncherActivity.this.finish();
            }
        },4000);
    }

}
