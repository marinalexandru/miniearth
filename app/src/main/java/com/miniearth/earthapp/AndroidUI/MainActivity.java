package com.miniearth.earthapp.AndroidUI;

import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.miniearth.earthapp.Graphics2D.HeatMapFactory;
import com.miniearth.earthapp.R;
import com.miniearth.earthapp.Graphics3D.AppSurfaceView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ChangeTextureOnSlideActivity {


    private DrawerLayout drawerLayout;
    private List<DrawerItemModel> menuModelList;

    /**
     * Cannot set content view directly because android cant inflate UI level and OpenGL level at same time for our Earth.
     * setContentView(R.layout.ui_main) resulted in crash Error inflating EarthSurfaceView
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RelativeLayout earthLayout = new RelativeLayout(this);
        addOpenGlLevel(earthLayout);
        addAndroidUILevel(earthLayout);
        setContentView(earthLayout);
        setNavigationDrawer();
        findViewById(R.id.autoRotateButton).performClick(); /*default rotation mode*/
    }

    private void addOpenGlLevel(RelativeLayout earthLayout) {
        appGLView = new AppSurfaceView(this);
        earthLayout.addView(appGLView);
    }

    private void addAndroidUILevel(RelativeLayout earthLayout) {
        RelativeLayout androidUI = (RelativeLayout) getLayoutInflater().inflate(R.layout.ui_main, null);
        earthLayout.addView(androidUI);
    }


    //--click listeners from UI
    public void autoRotate(View view) {
        findViewById(R.id.autoRotateButton).setSelected(true);
        findViewById(R.id.freeRotateButton).setSelected(false);
        findViewById(R.id.lockButton).setSelected(false);
        setAutoRotate();
    }

    private void setAutoRotate() {
        appGLView.setAutoRotate();
    }

    public void freeRotate(View view) {
        findViewById(R.id.autoRotateButton).setSelected(false);
        findViewById(R.id.freeRotateButton).setSelected(true);
        findViewById(R.id.lockButton).setSelected(false);
        setFreeRotate();
    }

    private void setFreeRotate() {
        appGLView.setFreeRotate();
    }

    public void lockToPosition(View view) {
        findViewById(R.id.autoRotateButton).setSelected(false);
        findViewById(R.id.freeRotateButton).setSelected(false);
        findViewById(R.id.lockButton).setSelected(true);
        setLockPosition();
    }

    private void setLockPosition(){
        appGLView.setLockIntoPosition();
    }

    private void setNavigationDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {

            }

            @Override
            public void onDrawerOpened(View view) {
                findViewById(R.id.menuButton).setSelected(true);
            }

            @Override
            public void onDrawerClosed(View view) {
                findViewById(R.id.menuButton).setSelected(false);
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

        final ListView menuListView = (ListView) findViewById(R.id.menuListView);
        menuModelList = getMenuData();
        final SlidingDrawerAdapter adapter = new SlidingDrawerAdapter(this, menuModelList );
        menuListView.setAdapter(adapter);
        //initialise
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectPosition(position, adapter);
            }
        });
    }

    private void selectPosition(int position,SlidingDrawerAdapter adapter) {

        if (position!=0) {
            Toast.makeText(MainActivity.this, "Only temperature data available for Europe", Toast.LENGTH_LONG).show();
        }

        findViewById(R.id.slider_layout).setAlpha(1f);
        findViewById(R.id.slider_layout).setClickable(true);
        findViewById(R.id.slider_layout).setFocusable(true);

        applyTexture(menuModelList.get(position));
        adapter.notifyDataSetChanged();
        toggleMenu(findViewById(R.id.menuButton));
        ((TextView)findViewById(R.id.measurement_indicator)).setText(menuModelList.get(position).getName());
    }

    private void applyTexture(DrawerItemModel drawerItemModel) {
        clearSelection();
        drawerItemModel.setSelected(true);
        loadHeatMaps(drawerItemModel.readingType);
        setSeekBar(findViewById(R.id.day_seek_bar));
    }

    private void clearSelection() {
        for (DrawerItemModel itemModel : menuModelList){
            itemModel.setSelected(false);
        }
    }

    private List<DrawerItemModel> getMenuData() {
        List<DrawerItemModel> drawerItemModels = new ArrayList<>();
        drawerItemModels.add(new DrawerItemModel("Temperature",R.drawable.rsz_thumb_earth_normal,HeatMapFactory.TEMPERATURE));
        drawerItemModels.add(new DrawerItemModel("Humidity",R.drawable.rsz_thumb_earth_bw_clouds, HeatMapFactory.HUMIDITY));
        drawerItemModels.add(new DrawerItemModel("Wind speed",R.drawable.rsz_thumb_earth_clouds, HeatMapFactory.WINDSPEED));
        drawerItemModels.add(new DrawerItemModel("Precipitation chance",R.drawable.rsz_thumb_earth_simple, HeatMapFactory.PRECIPITATION_CHANCE));
        drawerItemModels.add(new DrawerItemModel("Pressure",R.drawable.rsz_thumb_earth_simple_bw_enhanced, HeatMapFactory.PRESSURE));
        return drawerItemModels;
    }

    public void toggleMenu(View view){
        if (!drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.openDrawer(Gravity.LEFT);
        }else {
            drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }

    @Override
    protected void updateTimeDisplay(String time) {
        super.updateTimeDisplay(time);
        ((TextView) findViewById(R.id.date_indicator)).setText(time);
    }

    @Override
    protected void displayValueOnLegend(int min, int max) {
        super.displayValueOnLegend(min, max);
        ((TextView) findViewById(R.id.max_indicator)).setText("Max: "+max);
        ((TextView) findViewById(R.id.min_indicator)).setText("Min: "+min);
    }

    @Override
    protected void onResume() {
        super.onResume();
    //    appGLView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    //    appGLView.onPause();
    }
}
