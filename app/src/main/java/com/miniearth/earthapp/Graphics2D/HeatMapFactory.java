package com.miniearth.earthapp.Graphics2D;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.Log;

import com.miniearth.earthapp.Models3D.Earth;
import com.miniearth.earthapp.WeatherLogicAndSync.WeatherReading;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kath on 7/5/15.
 */
public class HeatMapFactory {

    private final Bitmap initialBitmap;
    Context c;
    public static int MARKER_BITMAP_WIDTH = 33;
    public static int MARKER_BITMAP_HEIGHT = 33;
    Map<Point, WeatherReading> pointCoordinateMap;
    List<HeatMapLibrary.HeatPoint> heatPoints;

    public static final int TEMPERATURE = 0;
    public static final int PRESSURE = 1;
    public static final int WINDSPEED = 2;
    public static final int HUMIDITY = 3;
    public static final int PRECIPITATION_CHANCE = 4;

    private int READING_TYPE = -99;

    public HeatMapFactory(Context c, Bitmap initialBitmap, int READING_TYPE, List<WeatherReading> coordinateList) {
        this.c = c;
        this.READING_TYPE = READING_TYPE;
        this.initialBitmap = initialBitmap;
        CoordinateMapper coordinateMapper = new CoordinateMapper(initialBitmap);
        pointCoordinateMap = coordinateMapper.getGeographicalMapping(coordinateList);
        heatPoints = new ArrayList<>();
        for (Map.Entry<Point, WeatherReading> pointCoordinateEntry : pointCoordinateMap.entrySet()) {
            Log.d("heat_map", "(" + pointCoordinateEntry.getKey().x + "," + pointCoordinateEntry.getKey().y + ")" + " " +
                    "(" + pointCoordinateEntry.getValue().getLatitude() + "," + pointCoordinateEntry.getValue().getLongitude() + ")");
            heatPoints.add(new HeatMapLibrary.HeatPoint(pointCoordinateEntry.getKey(),pointCoordinateEntry.getValue()));
        }
    }

    public Bitmap getMarkedBitmap() {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        final Bitmap markerBitmap = BitmapFactory.decodeResource(c.getResources(), Earth.getEarthMarkerResource(), options);
        final Bitmap scaledSquareBitmap = Bitmap.createScaledBitmap(markerBitmap, MARKER_BITMAP_WIDTH, MARKER_BITMAP_HEIGHT, false);
        return overlay(initialBitmap, scaledSquareBitmap);
//        HeatMapLibrary.HeatTask heatTask = new HeatMapLibrary.HeatTask(initialBitmap.getWidth(), initialBitmap.getHeight(), 2000f, heatPoints);
//        return overlay(initialBitmap, heatTask.getHeatBitMap());
    }


    //docs:
    private Bitmap overlay(Bitmap bmp1, Bitmap scaledSquareBitmap) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, 0, 0, null);
        for (Map.Entry<Point, WeatherReading> pointCoordinateEntry : pointCoordinateMap.entrySet()) {
            Bitmap recoloredBitmap = changeBitmapColor(scaledSquareBitmap, calculateColorFrom(pointCoordinateEntry.getValue()));
            double ratioXtoY = CoordinateMapper.calculateXToYRatio(pointCoordinateEntry.getValue().getLatitude());
            int local_marker_width = (int) ((double) MARKER_BITMAP_WIDTH * ratioXtoY);
            final Bitmap geoProjectionScaledMarker = Bitmap.createScaledBitmap(recoloredBitmap, local_marker_width, MARKER_BITMAP_HEIGHT, false);
            canvas.drawBitmap(geoProjectionScaledMarker, pointCoordinateEntry.getKey().x - local_marker_width, pointCoordinateEntry.getKey().y - MARKER_BITMAP_HEIGHT, null);
        }
        return bmOverlay;
    }

    private int calculateColorFrom(WeatherReading weatherReading) {

        float value = weatherReading.getNormalisedDisplayReading();

//        int NUM_COLORS = 4;
//        float[][] color = {{0, 0, 1}, {0, 1, 0}, {1, 1, 0}, {1, 0, 0}};
        int NUM_COLORS = 5;
        float[][] color = {{0, 0, 1},  //blue
                {0, 1, 1},//cyan
                {0, 1, 0},//green
                {1, 1, 0},//yellow
                {1, 0, 0}//red
        };
        int idx1;        // |-- Our desired color will be between these two indexes in "color".
        int idx2;        // |
        float fractBetween = 0;  // Fraction between "idx1" and "idx2" where our value is.

        if (value <= 0) {
            idx1 = idx2 = 0;
        }    // accounts for an input <=0
        else if (value >= 1) {
            idx1 = idx2 = NUM_COLORS - 1;
        }    // accounts for an input >=0
        else {
            value = value * (NUM_COLORS - 1);        // Will multiply value by 3.
            idx1 = (int) Math.floor(value);                  // Our desired color will be after this index.
            idx2 = idx1 + 1;                        // ... and before this index (inclusive).
            fractBetween = value - idx1;    // Distance between the two indexes (0-1).
        }

        float red = ((color[idx2][0] - color[idx1][0]) * fractBetween + color[idx1][0]);
        float green = (color[idx2][1] - color[idx1][1]) * fractBetween + color[idx1][1];
        float blue = (color[idx2][2] - color[idx1][2]) * fractBetween + color[idx1][2];

        Log.d("legelnd", "rgb: " + red + ", " + green + ", " + blue);

        return Color.argb(255, (int) (red * 255f), (int) (green * 255f), (int) (blue * 255f));
    }

    private Bitmap changeBitmapColor(Bitmap sourceBitmap, int color) {
        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0,
                sourceBitmap.getWidth() - 1, sourceBitmap.getHeight() - 1);
        Paint p = new Paint();
        ColorFilter filter = new LightingColorFilter(color, 1);
        p.setColorFilter(filter);
        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, p);
        return resultBitmap;
    }

}
