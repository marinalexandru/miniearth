package com.miniearth.earthapp.Graphics2D;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.Log;

import com.miniearth.earthapp.WeatherLogicAndSync.WeatherReading;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kath on 7/5/15.
 */
public class CoordinateMapper {


    private final int width;
    private final int height;

    public CoordinateMapper(Bitmap initialImage) {
        width = initialImage.getWidth();
        height = initialImage.getHeight();
    }

    public Map<Point, WeatherReading> getGeographicalMapping(List<WeatherReading> coordinateList) {
        Map<Point, WeatherReading> pointCoordinateMap = new HashMap<>();
        for (WeatherReading reading : coordinateList) {
            Point mapXY = degreesToPixels(reading);
            pointCoordinateMap.put(mapXY, reading);
        }
        return pointCoordinateMap;
    }

    //http://stackoverflow.com/questions/14329691/covert-latitude-longitude-point-to-a-pixels-x-y-on-mercator-projection?lq=1

    /**
     * The Mercator map projection is a special limiting case of the Lambert Conic Conformal map projection with the equator as the single standard parallel. All other parallels of latitude are straight lines and the meridians are also straight lines at right angles to the equator, equally spaced. It is the basis for the transverse and oblique forms of the projection. It is little used for land mapping purposes but is in almost universal use for navigation charts. As well as being conformal, it has the particular property that straight lines drawn on it are lines of constant bearing. Thus navigators may derive their course from the angle the straight course line makes with the meridians.
     *
     * @param reaading custom object for store latitude and longitude information
     * @return latitude and longitude coords
     * @see <a href="http://stackoverflow.com/questions/14329691/covert-latitude-longitude-point-to-a-pixels-x-y-on-mercator-projection?lq=1">http://stackoverflow.com</a>
     */
    public Point degreesToPixels(WeatherReading reaading) {
        double lat = reaading.getLatitude();
        double lng = reaading.getLongitude();
        int x;
        int y;
        x = calculateXPointFromLongitude(lng, width);
        y = calculateYPointFromLatitude(lat, height);
        return new Point(x, y);
    }


    public static int calculateXPointFromLongitude(double lng, int width) {
        return (int) ((lng + 180) * ((double) width / 360));
    }

    public static int calculateYPointFromLatitude(double lat, int height) {
        return (int) (((lat * -1) + 90) * ((double) height / 180));
    }

    public static double calculateXToYRatio(double lat) {
        return (Math.abs(lat)+90)/90;
    }


    public static class Coordinate {
        double latitude;
        double longitude;

        public Coordinate(double lat, double lon) {
            this.latitude = lat;
            this.longitude = lon;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }

}
