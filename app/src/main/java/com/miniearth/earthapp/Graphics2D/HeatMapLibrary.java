package com.miniearth.earthapp.Graphics2D;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RadialGradient;
import android.graphics.Shader;

import com.miniearth.earthapp.WeatherLogicAndSync.WeatherReading;

import java.util.List;

/**
 * Created by kath on 8/22/15.
 */
public class HeatMapLibrary  {

    public static class HeatPoint{
        Point point;
        WeatherReading weatherReading;

        public HeatPoint(Point point, WeatherReading weatherReading){
            this.weatherReading = weatherReading;
            this.point = point;
        }

        public Point getPoint() {
            return point;
        }

        public WeatherReading getWeatherReading() {
            return weatherReading;
        }
    }


    public static class HeatTask {


        private Canvas myCanvas;
        private Bitmap heatBitMap;
        private int width;
        private int height;
        private float radius;
        private List<HeatPoint> points;

        public HeatTask(int width, int height, float radius, List<HeatPoint> points){
            heatBitMap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            myCanvas = new Canvas(heatBitMap);
            Paint p = new Paint();
            p.setStyle(Paint.Style.FILL);
            p.setColor(Color.TRANSPARENT);
            this.width = width;
            this.height = height;
            this.points = points;
            myCanvas.drawRect(0, 0, width, height, p);
            this.radius = radius;
        }

        public Bitmap getHeatBitMap(){
            for(HeatPoint p : points){
                addPoint(p.point.x, p.point.y, p.getWeatherReading().getDisplayedField());
            }
            colorize();
            return heatBitMap;
        }



        private void addPoint(float x, float y, int times) {
            RadialGradient g = new RadialGradient(x, y, radius, Color.argb(
                    Math.max(10 * times, 255), 0, 0, 0), Color.TRANSPARENT,
                    Shader.TileMode.CLAMP);
            Paint gp = new Paint();
            gp.setShader(g);
            myCanvas.drawCircle(x, y, radius, gp);
        }

        private void colorize() {
            int[] pixels = new int[(int) (this.width * this.height)];
            heatBitMap.getPixels(pixels, 0, this.width, 0, 0, this.width,
                    this.height);


            for (int i = 0; i < pixels.length; i++) {
                int r = 0, g = 0, b = 0, tmp = 0;
                int alpha = pixels[i] >>> 24;
                if (alpha == 0) {
                    continue;
                }
                if (alpha <= 255 && alpha >= 235) {
                    tmp = 255 - alpha;
                    r = 255 - tmp;
                    g = tmp * 12;
                } else if (alpha <= 234 && alpha >= 200) {
                    tmp = 234 - alpha;
                    r = 255 - (tmp * 8);
                    g = 255;
                } else if (alpha <= 199 && alpha >= 150) {
                    tmp = 199 - alpha;
                    g = 255;
                    b = tmp * 5;
                } else if (alpha <= 149 && alpha >= 100) {
                    tmp = 149 - alpha;
                    g = 255 - (tmp * 5);
                    b = 255;
                } else
                    b = 255;
                pixels[i] = Color.argb((int) alpha / 2, r, g, b);
            }
            heatBitMap.setPixels(pixels, 0, this.width, 0, 0, this.width,
                    this.height);
        }
    }
}
