package com.miniearth.earthapp.Models3D;

import android.content.Context;
import android.opengl.Matrix;
import android.widget.TextView;

import com.miniearth.earthapp.AndroidUI.MainActivity;
import com.miniearth.earthapp.R;
import com.miniearth.earthapp.Utils.EarthConstants;

/**
 * Created by kath on 6/14/2015.
 */
public class EarthDisplayManager {

    protected final float[] mMVPMatrix = new float[16];
    protected final float[] mProjectionMatrix = new float[16];
    protected final float[] mViewMatrix = new float[16];
    protected float zoomScaleFactor = 0.45f;

    public float[] getMVPMatrix() {
        return mMVPMatrix;
    }

    /**
     * Should be called from top implementation (Surface view) and sets the zoomScaleFactor determined from touch inputs.
     * <b>Observation.</b> <br/>
     * The zoomScaleFactor will also influence the rotation. It is proportional with the magnitude of the rotation.
     */
    public final void setZoomScaleFactor(float mult) {
        if (mult == 0)
            return;
        zoomScaleFactor *= mult;
        if (zoomScaleFactor < EarthConstants.RENDER_CONSTANTS.MIN_ZOOM_SCALE_FACTOR)
            zoomScaleFactor = EarthConstants.RENDER_CONSTANTS.MIN_ZOOM_SCALE_FACTOR;
        if (zoomScaleFactor > EarthConstants.RENDER_CONSTANTS.MAX_ZOOM_SCALE_FACTOR)
            zoomScaleFactor = EarthConstants.RENDER_CONSTANTS.MAX_ZOOM_SCALE_FACTOR;
    }

    protected final float getZoomScaleFactor(){
        return zoomScaleFactor;
    }

    public void addProjectionAndZoomControl(final Context context,float ratio){
        // Camera and projection relative to window dimensions
        Matrix.frustumM(mProjectionMatrix, 0,
                -ratio * zoomScaleFactor, ratio * zoomScaleFactor,
                -1 * zoomScaleFactor, 1 * zoomScaleFactor,
                EarthConstants.RENDER_CONSTANTS.NEAR_PLANE,
                EarthConstants.RENDER_CONSTANTS.FAR_PLANE);

        ((MainActivity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView)((MainActivity)context).findViewById(R.id.zoom_monitor)).setText(String.format( "Zoom:  %.2f",  1/zoomScaleFactor));
                ((TextView)((MainActivity)context).findViewById(R.id.scale_monitor)).setText(String.format( "Scale: %.2f",  zoomScaleFactor));
            }
        });

        Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -2.52f, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);
    }


}
