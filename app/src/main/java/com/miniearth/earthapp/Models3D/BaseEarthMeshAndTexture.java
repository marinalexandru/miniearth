package com.miniearth.earthapp.Models3D;

import com.miniearth.earthapp.Utils.EarthMath;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kath on 6/8/2015.
 */
public class BaseEarthMeshAndTexture {
    protected static final int MAX_DETAIL = 5;
    protected static final int VERTEX_NUMBER = 5;
    protected  int numStrips;
    protected static final int NUMBERS_PER_VERTEX = 3; //3d coordinate
    protected static final int NUMBERS_PER_TEXTURE = 2; //2d coordinate
    protected final List<FloatBuffer> verticesBuffer = new ArrayList<>();
    protected final List<float[]> mVertices = new ArrayList<>();
    protected final List<FloatBuffer> texturesBuffer = new ArrayList<>();
    protected int[] textures = new int[1];
    protected final int vStride = NUMBERS_PER_VERTEX * 4; // 4 bytes per vertex
    protected final int tStride = NUMBERS_PER_TEXTURE * 4; // 4 bytes per vertex
    protected void loadGeometryAndTexture(final int depth, final float radius){
        final int normalisedDepth = Math.max(1, Math.min(MAX_DETAIL, depth));
        this.numStrips = (int) (Math.pow(2, normalisedDepth - 1) * VERTEX_NUMBER);
        final int verticesPerStrip = (int) (Math.pow(2, normalisedDepth) * 3);
        final double altitudeStepAngle = EarthMath.ONE_TWENTY_DEGREES / Math.pow(2, normalisedDepth);
        final double azimuthStepAngle = EarthMath.THREE_SIXTY_DEGREES / this.numStrips;
        double x, y, z, h, altitude, azimuth;
        final List<float[]> texture = new ArrayList<>();

        for (int stripNum = 0; stripNum < this.numStrips; stripNum++) {
            final float[] vertices = new float[verticesPerStrip * NUMBERS_PER_VERTEX];
            final float[] texturePoints = new float[verticesPerStrip * NUMBERS_PER_TEXTURE];
            int vertexPos = 0;
            int texturePos = 0;

            altitude = EarthMath.NINETY_DEGREES;
            azimuth = stripNum * azimuthStepAngle;

            for (int vertexNum = 0; vertexNum < verticesPerStrip; vertexNum += 2) {
                // First point - Vertex.
                y = radius * Math.sin(altitude);
                h = radius * Math.cos(altitude);
                z = h * Math.sin(azimuth);
                x = h * Math.cos(azimuth);
                vertices[vertexPos++] = (float) x;
                vertices[vertexPos++] = (float) y;
                vertices[vertexPos++] = (float) z;

                // First point - Texture.
                texturePoints[texturePos++] = (float) (1 - azimuth / EarthMath.THREE_SIXTY_DEGREES);
                texturePoints[texturePos++] = (float) (1 - (altitude + EarthMath.NINETY_DEGREES) / EarthMath.ONE_EIGHTY_DEGREES);

                // Second point - Vertex.
                altitude -= altitudeStepAngle;
                azimuth -= azimuthStepAngle / 2.0;
                y = radius * Math.sin(altitude);
                h = radius * Math.cos(altitude);
                z = h * Math.sin(azimuth);
                x = h * Math.cos(azimuth);
                vertices[vertexPos++] = (float) x;
                vertices[vertexPos++] = (float) y;
                vertices[vertexPos++] = (float) z;

                // Second point - Texture.
                texturePoints[texturePos++] = (float) (1 - azimuth / EarthMath.THREE_SIXTY_DEGREES);
                texturePoints[texturePos++] = (float) (1 - (altitude + EarthMath.NINETY_DEGREES) / EarthMath.ONE_EIGHTY_DEGREES);

                azimuth += azimuthStepAngle;
            }

            this.mVertices.add(vertices);
            texture.add(texturePoints);
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(verticesPerStrip * NUMBERS_PER_VERTEX * Float.SIZE);
            byteBuffer.order(ByteOrder.nativeOrder());
            FloatBuffer fb = byteBuffer.asFloatBuffer();
            fb.put(this.mVertices.get(stripNum));
            fb.position(0);
            this.verticesBuffer.add(fb);

            // Setup texture.
            byteBuffer = ByteBuffer.allocateDirect(verticesPerStrip * NUMBERS_PER_TEXTURE * Float.SIZE);
            byteBuffer.order(ByteOrder.nativeOrder());
            fb = byteBuffer.asFloatBuffer();
            fb.put(texture.get(stripNum));
            fb.position(0);
            this.texturesBuffer.add(fb);
        }
    }


}
