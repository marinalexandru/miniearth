package com.miniearth.earthapp.Models3D;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import com.miniearth.earthapp.R;

/**
 * Created by kath on 6/4/2015.
 */
public class BaseEarthShader extends BaseEarthMeshAndTexture {

    String vertexShaderCode =
            "";

    String fragmentShaderCode =
            "";
    protected int mMVPMatrixHandle;
    protected int mPositionHandle;
    protected int mTextureCoordinateHandle;

    protected void loadShaders(Context c) {
        //get from local android resources
        vertexShaderCode = c.getResources().getString(R.string.vertexShader);
        fragmentShaderCode = c.getResources().getString(R.string.fragmentShader);
        //get fragment, vertex and program handles
        int vertexShaderHandle =  loadVertexShader();
        int fragmentShaderHandle =  loadFragmentShader();
        int program = createShaderProgram(vertexShaderHandle,fragmentShaderHandle);

        //get global variables from the shader programs
        mMVPMatrixHandle = GLES20.glGetUniformLocation(program, "u_MVPMatrix");
        mPositionHandle = GLES20.glGetAttribLocation(program, "a_Position");
        mTextureCoordinateHandle = GLES20.glGetAttribLocation(program, "a_TexCoordinate");

        GLES20.glUseProgram(program);
    }

    private int createShaderProgram(int vertexShaderHandle, int fragmentShaderHandle) {
        int shaderProgram = GLES20.glCreateProgram();
            GLES20.glAttachShader(shaderProgram, vertexShaderHandle);
            GLES20.glAttachShader(shaderProgram, fragmentShaderHandle);
            GLES20.glBindAttribLocation(shaderProgram, 0, "a_Position");
            GLES20.glBindAttribLocation(shaderProgram, 1, "a_TexCoordinate");
            GLES20.glLinkProgram(shaderProgram);
            int[] linkSuccessful = new int[1];
            GLES20.glGetProgramiv(shaderProgram, GLES20.GL_LINK_STATUS, linkSuccessful, 0);
            if (linkSuccessful[0] == 0) {
                GLES20.glDeleteProgram(shaderProgram);
                Log.d("shader_fail", GLES20.glGetProgramInfoLog(shaderProgram));
            }
        return shaderProgram;
    }

    private int loadFragmentShader() {
        int fragmentShaderHandle = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
        GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode);
        GLES20.glCompileShader(fragmentShaderHandle);
        final int[] fragmentShaderCompileStatus = new int[1];
        GLES20.glGetShaderiv(fragmentShaderHandle, GLES20.GL_COMPILE_STATUS, fragmentShaderCompileStatus, 0);
        if (fragmentShaderCompileStatus[0] == 0) {
            Log.d("shader_fail", GLES20.glGetShaderInfoLog(fragmentShaderHandle));
            GLES20.glDeleteShader(fragmentShaderHandle);
        }
        return fragmentShaderHandle;
    }

    private int loadVertexShader() {
        int vertexShaderHandle = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        GLES20.glShaderSource(vertexShaderHandle, vertexShaderCode);
        GLES20.glCompileShader(vertexShaderHandle);
        final int[] vertexShaderCompiledStatus = new int[1];
        GLES20.glGetShaderiv(vertexShaderHandle, GLES20.GL_COMPILE_STATUS, vertexShaderCompiledStatus, 0);
        if (vertexShaderCompiledStatus[0] == 0) {
            Log.d("shader_fail", GLES20.glGetShaderInfoLog(vertexShaderHandle));
            GLES20.glDeleteShader(vertexShaderHandle);
        }
        return vertexShaderHandle;
    }
}
