package com.miniearth.earthapp.Models3D;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;

import com.miniearth.earthapp.R;

/**
 * Created by kath on 6/1/2015.
 */
public class Earth extends BaseEarthShader {

    public static int getEarthTextureResource(){
//        return R.drawable.earth_normal;
        return R.drawable.earth_texture_mid2;
    }

    public static int getEarthMarkerResource(){
        return R.drawable.gps_marker4;
    }

    private EarthDisplayManager mEarthDisplayManager;
    private EarthRotationManager mEarthRotationManager;
    private Context context;
    public Earth(final int depth, final float radius, Context context) {
        this.context = context;
        loadGeometryAndTexture(depth, radius);
        loadShaders(context);
        mEarthDisplayManager = new EarthDisplayManager();
        mEarthRotationManager = new EarthRotationManager();
        initRotationControl();

    }

    public EarthDisplayManager getEarthDisplayManager(){
        return mEarthDisplayManager;
    }

    public EarthRotationManager getEarthRotationManager(){
        return mEarthRotationManager;
    }

    public void loadGLTexture(final Context context, final int texture) {
        GLES20.glGenTextures(1, textures, 0);
        if (textures[0] != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), texture, options);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0]);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
            bitmap.recycle();
        }

        if (textures[0] == 0) {
            throw new RuntimeException("Texture load fail");
        }
    }

    public void loadBitmapGLTexture(Bitmap bitmap) {
        GLES20.glGenTextures(1, textures, 0);
        if (textures[0] != 0) {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0]);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
            //bitmap.recycle();
        }

        if (textures[0] == 0) {
            throw new RuntimeException("Texture load fail");
        }
    }


    public void clearGLTextures() {
        GLES20.glDeleteTextures(1, textures, 0);
    }

    public void initRotationControl() {

    }

    public void draw(float ratio) {
        // Pinch ui control for zoom and MVP matrix
        // gets calculated here with respect to the
        // screen aspect ratio.
        // RESULTS: MVP MATRIX
        this.getEarthDisplayManager().addProjectionAndZoomControl(context, ratio);

        // X,Y plane rotations are calculated here
        // based on simple touch input from the user.
        // Also the pinch rotate control induces a
        // Z axis rotation that is also calculated here.
        // RESULTS: mTotalRotationMatrix
        this.getEarthRotationManager().addRotationControl(this.getEarthDisplayManager().getZoomScaleFactor());

        // Perform the actual draw
        // based on the above method calls.
        drawTrianglesAndTexture(getEarthCanvasMatrix());
    }

    private float[] getEarthCanvasMatrix() {
        float[] canvasMatrix = new float[16];
        Matrix.multiplyMM(canvasMatrix, 0, getEarthDisplayManager().getMVPMatrix(), 0, getEarthRotationManager().getTotalRotationMatrix(), 0);
        return canvasMatrix;
    }

    private void drawTrianglesAndTexture(float[] mvpMatrix) {
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, this.textures[0]);
        GLES20.glFrontFace(GLES20.GL_CW);
        for (int i = 0; i < this.numStrips; i++) {
            //vertex
            this.verticesBuffer.get(i).position(0);
            GLES20.glVertexAttribPointer(mPositionHandle, NUMBERS_PER_VERTEX,
                    GLES20.GL_FLOAT, false,
                    vStride, this.verticesBuffer.get(i));
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            //texture
            this.texturesBuffer.get(i).position(0);
            GLES20.glVertexAttribPointer(mTextureCoordinateHandle, NUMBERS_PER_TEXTURE,
                    GLES20.GL_FLOAT, false,
                    tStride, this.texturesBuffer.get(i));
            GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);
            //draw strip
            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, this.mVertices.get(i).length / NUMBERS_PER_VERTEX);
        }

        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
        // Disable the client state before leaving.
        GLES20.glDisableVertexAttribArray(mPositionHandle);
        GLES20.glDisableVertexAttribArray(mTextureCoordinateHandle);
    }
}

