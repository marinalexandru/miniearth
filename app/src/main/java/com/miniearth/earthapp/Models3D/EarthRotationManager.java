package com.miniearth.earthapp.Models3D;

import android.opengl.Matrix;

/**
 * Created by kath on 6/14/2015.
 */
public class EarthRotationManager {


    protected float[] mTotalRotationMatrix = new float[16];
    protected boolean autoRotate = false;
    protected boolean lockSceene = false;
    protected float rotationAngleX = 0;
    protected float rotationAngleY = 0;

    protected float rotationAngleZ = 0;

    public float[] getTotalRotationMatrix() {
        return mTotalRotationMatrix;
    }

    public EarthRotationManager(){
        Matrix.setIdentityM(mTotalRotationMatrix, 0);
    }

    public float getRotationAngleX() {
        return rotationAngleX;
    }

    public float getRotationAngleY() {
        return rotationAngleY;
    }

    public void autoRotate() {
        autoRotate = true;
        lockSceene = false;
    }

    public void lockPosition() {
        lockSceene = true;
    }

    public void freeRotate() {
        autoRotate = false;
        lockSceene = false;
    }

    public void setRotationAngleX(float rotationAngleX) {
        this.rotationAngleX = rotationAngleX;
    }

    public void setRotationAngleY(float rotationAngleY) {
        this.rotationAngleY = rotationAngleY;
    }

    public void setRotationAngleZ(float rotationAngleZ) {
        this.rotationAngleZ = rotationAngleZ;
    }

    protected void addRotationControl(float rotationScaleFactor) {
        float[] mXRotationMatrix = new float[16];
        float[] mYRotationMatrix = new float[16];
        float[] mZRotationMatrix = new float[16];
        if (lockSceene) {
            Matrix.setRotateM(mYRotationMatrix, 0, 0 * rotationScaleFactor, 0, 1, 0);
            Matrix.setRotateM(mXRotationMatrix, 0, 0 * rotationScaleFactor, 1, 0, 0);
        } else {
            if (autoRotate) {
                Matrix.setRotateM(mYRotationMatrix, 0, 0.75f * rotationScaleFactor, 0, 1, 0);
                Matrix.setRotateM(mXRotationMatrix, 0, -0.75f * rotationScaleFactor, 1, 0, 0);
            } else {
                Matrix.setRotateM(mYRotationMatrix, 0, rotationAngleX * rotationScaleFactor * 0.4f, 0, 1, 0);
                Matrix.setRotateM(mXRotationMatrix, 0, -rotationAngleY * rotationScaleFactor * 0.4f, 1, 0, 0);
            }
        }
        Matrix.setRotateM(mZRotationMatrix, 0, -rotationAngleZ, 0, 0, 1);
        float[] mRotationMatrix = new float[16];
        float[] mTmpMatrix = new float[16];
        if (rotationAngleZ != 0) {
            System.arraycopy(mZRotationMatrix, 0, mRotationMatrix, 0, 16);
        } else {
            Matrix.multiplyMM(mRotationMatrix, 0, mXRotationMatrix, 0, mYRotationMatrix, 0);
        }
        Matrix.multiplyMM(mTmpMatrix, 0, mRotationMatrix, 0, mTotalRotationMatrix, 0);
        System.arraycopy(mTmpMatrix, 0, mTotalRotationMatrix, 0, 16);
        rotationAngleX = 0;
        rotationAngleY = 0;
        rotationAngleZ = 0;
    }
}
