package com.miniearth.earthapp.Utils;

import android.util.FloatMath;
import android.view.MotionEvent;

/**
 * Created by kath on 6/1/2015.
 */
public class EarthMath {

    /** 180 in radians. */
    public static final double ONE_EIGHTY_DEGREES = Math.PI;

    /** 360 in radians. */
    public static final double THREE_SIXTY_DEGREES = ONE_EIGHTY_DEGREES * 2;

    /** 120 in radians. */
    public static final double ONE_TWENTY_DEGREES = THREE_SIXTY_DEGREES / 3;

    /** 90 degrees, North pole. */
    public static final double NINETY_DEGREES = Math.PI / 2;





    public static float getFingerSpan(MotionEvent touchEvent) {

        float x = touchEvent.getX(0) - touchEvent.getX(1);
        float y = touchEvent.getY(0) - touchEvent.getY(1);
        return FloatMath.sqrt(x * x + y * y);
    }

    public static float  getSpanAngle(MotionEvent touchEvent, float initialTouch1X, float initialTouch1Y,float initialTouch2X, float initialTouch2Y) {
        float touch2X, touch2Y, touch1X, touch1Y;
        touch1X = touchEvent.getX(0);
        touch1Y = touchEvent.getY(0);
        touch2X = touchEvent.getX(1);
        touch2Y = touchEvent.getY(1);

        float angle1 = (float) Math.atan2((initialTouch2Y - initialTouch1Y), (initialTouch2X - initialTouch1X));
        float angle2 = (float) Math.atan2((touch2Y - touch1Y), (touch2X - touch1X));

        float angle = ((float) Math.toDegrees(angle1 - angle2)) % 360;
        if (angle < -180.f) angle += 360.0f;
        if (angle > 180.f) angle -= 360.0f;
        return angle;
    }
}
