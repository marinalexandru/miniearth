package com.miniearth.earthapp.Utils;

/**
 * Created by kath on 5/31/2015.
 */
public class EarthConstants {
    //debug tags
    public static class DEBUG{
        public static String TAG_GESTURES_INFO = "GESTURES_INFO";
    }

    //SurfaceView constants
    public static class SURFACE_VIEW_CONSTANTS{
        public static final float TOUCH_SCALE_FACTOR = 180.0f / 320;
        public static final int OPEN_GL_ES_2_0_VERSION = 2;
        public static int ONE_TOUCH_ON_SCREEN = 1;
        public static int SECOND_TOUCH_ON_SCREEN = 2;
        public static int CONSIDER_NO_TOUCH_ON_SCREEN = -999;
    }

    //Renderer constants
    public static class RENDER_CONSTANTS{
        public static final float MIN_ZOOM_SCALE_FACTOR = 0.1f;
        public static final float MAX_ZOOM_SCALE_FACTOR = 10f;
        public static final float NEAR_PLANE = 1f;
        public static final float FAR_PLANE = 10f;
    }
}
