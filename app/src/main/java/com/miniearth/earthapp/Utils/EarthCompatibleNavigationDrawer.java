package com.miniearth.earthapp.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.miniearth.earthapp.R;

/**
 * Created by kath on 6/8/2015.
 */
public class EarthCompatibleNavigationDrawer extends android.support.v4.widget.DrawerLayout {

    View mDrawerListView;

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mDrawerListView = findViewById(R.id.left_drawer);
    }

    public EarthCompatibleNavigationDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public EarthCompatibleNavigationDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public EarthCompatibleNavigationDrawer(Context context) {
        super(context);
    }
    //so we can pass touch events to childs
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if(event.getX() > 30 && event.getAction() == MotionEvent.ACTION_DOWN){
            if(isDrawerOpen(mDrawerListView) || isDrawerVisible(mDrawerListView)){
                return true;
            } else{
                return false;
            }
        }

        return true;
    }
}
