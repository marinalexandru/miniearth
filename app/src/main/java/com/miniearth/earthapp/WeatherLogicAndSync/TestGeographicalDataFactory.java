package com.miniearth.earthapp.WeatherLogicAndSync;

import com.miniearth.earthapp.Graphics2D.CoordinateMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kath on 7/11/15.
 */
public class TestGeographicalDataFactory {

    public static List<WeatherReading> createData(){
        List<WeatherReading> weatherReadings = new ArrayList<>();

        //DAY -1
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 10, 23, 23, 23, 23, "10/07/2012")); //north
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 25, 23, 23, 23, 23, "10/07/2012")); //romania
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 30, 23, 23, 23, 23, "10/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 20, 23, 23, 23, 23, "10/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 17, 23, 23, 23, 23, "10/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 23, 23, 23, 23, 23, "10/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 15, 23, 23, 23, 23, "10/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 14, 23, 23, 23, 23, "10/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 27, 23, 23, 23, 23, "10/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 25, 23, 23, 23, 23, "10/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 36, 23, 23, 23, 23, "10/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 33, 23, 23, 23, 23, "10/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 31, 23, 23, 23, 23, "10/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 27, 23, 23, 23, 23, "10/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 22, 23, 23, 23, 23, "10/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 38, 23, 23, 23, 23, "10/07/2012")); //South




        //DAY 0
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 10, 23, 23, 23, 23, "11/07/2012")); //north
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 28, 23, 23, 23, 23, "11/07/2012")); //romania
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 33, 23, 23, 23, 23, "11/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 23, 23, 23, 23, 23, "11/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 20, 23, 23, 23, 23, "11/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 26, 23, 23, 23, 23, "11/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 18, 23, 23, 23, 23, "11/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 17, 23, 23, 23, 23, "11/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 30, 23, 23, 23, 23, "11/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 22, 23, 23, 23, 23, "11/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 33, 23, 23, 23, 23, "11/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 30, 23, 23, 23, 23, "11/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 29, 23, 23, 23, 23, "11/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 24, 23, 23, 23, 23, "11/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 19, 23, 23, 23, 23, "11/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 38, 23, 23, 23, 23, "11/07/2012")); //South




        //DAY 1
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 10, 23, 23, 23, 23, "12/07/2012")); //north
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 28, 23, 23, 23, 23, "12/07/2012")); //romania
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 33, 23, 23, 23, 23, "12/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 23, 23, 23, 23, 23, "12/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 20, 23, 23, 23, 23, "12/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 26, 23, 23, 23, 23, "12/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 18, 23, 23, 23, 23, "12/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 17, 23, 23, 23, 23, "12/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 30, 23, 23, 23, 23, "12/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 25, 23, 23, 23, 23, "12/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 36, 23, 23, 23, 23, "12/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 33, 23, 23, 23, 23, "12/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 31, 23, 23, 23, 23, "12/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 27, 23, 23, 23, 23, "12/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 22, 23, 23, 23, 23, "12/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 38, 23, 23, 23, 23, "12/07/2012")); //South

//DAY 2
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 10, 23, 23, 23, 23, "13/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 36, 23, 23, 23, 23, "13/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 36, 23, 23, 23, 23, "13/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 36, 23, 23, 23, 23, "13/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 37, 23, 23, 23, 23, "13/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 22, 23, 23, 23, 23, "13/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 28, 23, 23, 23, 23, "13/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 23, 23, 23, 23, 23, "13/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 26, 23, 23, 23, 23, "13/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 29, 23, 23, 23, 23, "13/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 26, 23, 23, 23, 23, "13/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 18, 23, 23, 23, 23, "13/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 30, 23, 23, 23, 23, "13/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 17, 23, 23, 23, 23, "13/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 23, 23, 23, 23, 23, "13/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "13/07/2012")); //South
//DAY 3
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 10, 23, 23, 23, 23, "14/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 38, 23, 23, 23, 23, "14/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 38, 23, 23, 23, 23, "14/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 36, 23, 23, 23, 23, "14/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "14/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 27, 23, 23, 23, 23, "14/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 32, 23, 23, 23, 23, "14/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 28, 23, 23, 23, 23, "14/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 30, 23, 23, 23, 23, "14/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 30, 23, 23, 23, 23, "14/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 26, 23, 23, 23, 23, "14/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 18, 23, 23, 23, 23, "14/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 30, 23, 23, 23, 23, "14/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 17, 23, 23, 23, 23, "14/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 23, 23, 23, 23, 23, "14/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "14/07/2012")); //South

        // DAY 4
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 14, 23, 23, 23, 23, "15/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 38, 23, 23, 23, 23, "15/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 38, 23, 23, 23, 23, "15/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 36, 23, 23, 23, 23, "15/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "15/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 31, 23, 23, 23, 23, "15/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 36, 23, 23, 23, 23, "15/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 34, 23, 23, 23, 23, "15/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 34, 23, 23, 23, 23, "15/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 35, 23, 23, 23, 23, "15/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 27, 23, 23, 23, 23, "15/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 25, 23, 23, 23, 23, "15/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 30, 23, 23, 23, 23, "15/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 22, 23, 23, 23, 23, "15/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 23, 23, 23, 23, 23, "15/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "15/07/2012")); //South


 // DAY 5
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 14, 23, 23, 23, 23, "16/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 39, 23, 23, 23, 23, "16/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 39, 23, 23, 23, 23, "16/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 38, 23, 23, 23, 23, "16/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "16/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 34, 23, 23, 23, 23, "16/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 37, 23, 23, 23, 23, "16/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 36, 23, 23, 23, 23, "16/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 36, 23, 23, 23, 23, "16/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 35, 23, 23, 23, 23, "16/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 33, 23, 23, 23, 23, "16/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 27, 23, 23, 23, 23, "16/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 33, 23, 23, 23, 23, "16/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 25, 23, 23, 23, 23, "16/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 25, 23, 23, 23, 23, "16/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "16/07/2012")); //South

// DAY 6
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 14, 23, 23, 23, 23, "17/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 39, 23, 23, 23, 23, "17/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 39, 23, 23, 23, 23, "17/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 40, 23, 23, 23, 23, "17/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "17/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 36, 23, 23, 23, 23, "17/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 39, 23, 23, 23, 23, "17/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 38, 23, 23, 23, 23, "17/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 37, 23, 23, 23, 23, "17/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 37, 23, 23, 23, 23, "17/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 35, 23, 23, 23, 23, "17/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 28, 23, 23, 23, 23, "17/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 34, 23, 23, 23, 23, "17/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 27, 23, 23, 23, 23, "17/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 27, 23, 23, 23, 23, "17/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "17/07/2012")); //South

// DAY 7
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 14, 23, 23, 23, 23, "18/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 39, 23, 23, 23, 23, "18/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 39, 23, 23, 23, 23, "18/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 40, 23, 23, 23, 23, "18/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "18/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 38, 23, 23, 23, 23, "18/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 40, 23, 23, 23, 23, "18/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 40, 23, 23, 23, 23, "18/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 39, 23, 23, 23, 23, "18/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 39, 23, 23, 23, 23, "18/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 37, 23, 23, 23, 23, "18/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 30, 23, 23, 23, 23, "18/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 37, 23, 23, 23, 23, "18/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 30, 23, 23, 23, 23, "18/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 30, 23, 23, 23, 23, "18/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "18/07/2012")); //South

// DAY 8
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 20, 23, 23, 23, 23, "19/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 39, 23, 23, 23, 23, "19/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 39, 23, 23, 23, 23, "19/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 40, 23, 23, 23, 23, "19/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "19/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 39, 23, 23, 23, 23, "19/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 40, 23, 23, 23, 23, "19/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 40, 23, 23, 23, 23, "19/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 39, 23, 23, 23, 23, "19/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 39, 23, 23, 23, 23, "19/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 38, 23, 23, 23, 23, "19/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 32, 23, 23, 23, 23, "19/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 38, 23, 23, 23, 23, "19/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 32, 23, 23, 23, 23, "19/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 32, 23, 23, 23, 23, "19/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "19/07/2012")); //South

// DAY 9
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 21, 23, 23, 23, 23, "20/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 40, 23, 23, 23, 23, "20/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 41, 23, 23, 23, 23, "20/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 40, 23, 23, 23, 23, "20/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 41, 23, 23, 23, 23, "20/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 40, 23, 23, 23, 23, "20/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 40, 23, 23, 23, 23, "20/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 40, 23, 23, 23, 23, "20/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 40, 23, 23, 23, 23, "20/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 40, 23, 23, 23, 23, "20/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 39, 23, 23, 23, 23, "20/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 34, 23, 23, 23, 23, "20/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 39, 23, 23, 23, 23, "20/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 34, 23, 23, 23, 23, "20/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 34, 23, 23, 23, 23, "20/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "20/07/2012")); //South



// DAY 8
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 20, 23, 23, 23, 23, "21/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 39, 23, 23, 23, 23, "21/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 39, 23, 23, 23, 23, "21/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 40, 23, 23, 23, 23, "21/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "21/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 39, 23, 23, 23, 23, "21/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 40, 23, 23, 23, 23, "21/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 40, 23, 23, 23, 23, "21/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 39, 23, 23, 23, 23, "21/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 39, 23, 23, 23, 23, "21/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 38, 23, 23, 23, 23, "21/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 32, 23, 23, 23, 23, "21/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 38, 23, 23, 23, 23, "21/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 32, 23, 23, 23, 23, "21/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 32, 23, 23, 23, 23, "21/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "21/07/2012")); //South

        // DAY 7
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 14, 23, 23, 23, 23, "22/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 39, 23, 23, 23, 23, "22/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 39, 23, 23, 23, 23, "22/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 40, 23, 23, 23, 23, "22/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "22/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 38, 23, 23, 23, 23, "22/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 40, 23, 23, 23, 23, "22/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 40, 23, 23, 23, 23, "22/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 39, 23, 23, 23, 23, "22/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 39, 23, 23, 23, 23, "22/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 37, 23, 23, 23, 23, "22/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 30, 23, 23, 23, 23, "22/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 37, 23, 23, 23, 23, "22/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 30, 23, 23, 23, 23, "22/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 30, 23, 23, 23, 23, "22/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "22/07/2012")); //South


// DAY 6
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 14, 23, 23, 23, 23, "23/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 39, 23, 23, 23, 23, "23/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 39, 23, 23, 23, 23, "23/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 40, 23, 23, 23, 23, "23/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "23/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 36, 23, 23, 23, 23, "23/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 39, 23, 23, 23, 23, "23/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 38, 23, 23, 23, 23, "23/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 37, 23, 23, 23, 23, "23/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 37, 23, 23, 23, 23, "23/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 35, 23, 23, 23, 23, "23/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 28, 23, 23, 23, 23, "23/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 34, 23, 23, 23, 23, "23/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 27, 23, 23, 23, 23, "23/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 27, 23, 23, 23, 23, "23/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "23/07/2012")); //South





        // DAY 5
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 14, 23, 23, 23, 23, "24/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 39, 23, 23, 23, 23, "24/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 39, 23, 23, 23, 23, "24/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 38, 23, 23, 23, 23, "24/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "24/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 34, 23, 23, 23, 23, "24/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 37, 23, 23, 23, 23, "24/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 36, 23, 23, 23, 23, "24/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 36, 23, 23, 23, 23, "24/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 35, 23, 23, 23, 23, "24/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 33, 23, 23, 23, 23, "24/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 27, 23, 23, 23, 23, "24/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 33, 23, 23, 23, 23, "24/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 25, 23, 23, 23, 23, "24/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 25, 23, 23, 23, 23, "24/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "24/07/2012")); //South

        // DAY 4
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 14, 23, 23, 23, 23, "25/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 38, 23, 23, 23, 23, "25/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 38, 23, 23, 23, 23, "25/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 36, 23, 23, 23, 23, "25/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "25/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 31, 23, 23, 23, 23, "25/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 36, 23, 23, 23, 23, "25/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 34, 23, 23, 23, 23, "25/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 34, 23, 23, 23, 23, "25/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 35, 23, 23, 23, 23, "25/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 27, 23, 23, 23, 23, "25/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 25, 23, 23, 23, 23, "25/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 30, 23, 23, 23, 23, "25/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 22, 23, 23, 23, 23, "25/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 23, 23, 23, 23, 23, "25/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "25/07/2012")); //South



        //DAY 3
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 10, 23, 23, 23, 23, "26/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 38, 23, 23, 23, 23, "26/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 38, 23, 23, 23, 23, "26/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 36, 23, 23, 23, 23, "26/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 40, 23, 23, 23, 23, "26/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 27, 23, 23, 23, 23, "26/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 32, 23, 23, 23, 23, "26/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 28, 23, 23, 23, 23, "26/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 30, 23, 23, 23, 23, "26/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 30, 23, 23, 23, 23, "26/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 26, 23, 23, 23, 23, "26/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 18, 23, 23, 23, 23, "26/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 30, 23, 23, 23, 23, "26/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 17, 23, 23, 23, 23, "26/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 23, 23, 23, 23, 23, "26/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "26/07/2012")); //South



//DAY 2
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 10, 23, 23, 23, 23, "27/07/2012")); //north
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 36, 23, 23, 23, 23, "27/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 36, 23, 23, 23, 23, "27/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 36, 23, 23, 23, 23, "27/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 37, 23, 23, 23, 23, "27/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 22, 23, 23, 23, 23, "27/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 28, 23, 23, 23, 23, "27/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 23, 23, 23, 23, 23, "27/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 26, 23, 23, 23, 23, "27/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 29, 23, 23, 23, 23, "27/07/2012")); //romania
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 26, 23, 23, 23, 23, "27/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 18, 23, 23, 23, 23, "27/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 30, 23, 23, 23, 23, "27/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 17, 23, 23, 23, 23, "27/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 23, 23, 23, 23, 23, "27/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 41, 23, 23, 23, 23, "27/07/2012")); //South






        //DAY 1
        weatherReadings.add(new WeatherReading(1, "north                 ", new CoordinateMapper.Coordinate(71.15939142, 25.76293945), 1, 10, 23, 23, 23, 23, "28/07/2012")); //north
        weatherReadings.add(new WeatherReading(2, "romania               ", new CoordinateMapper.Coordinate(44.43333330, 26.10000000), 1, 28, 23, 23, 23, 23, "28/07/2012")); //romania
        weatherReadings.add(new WeatherReading(3, "Spania                ", new CoordinateMapper.Coordinate(40.00570800, -4.91103600), 1, 33, 23, 23, 23, 23, "28/07/2012")); //Spania
        weatherReadings.add(new WeatherReading(4, "Rusia eu              ", new CoordinateMapper.Coordinate(55.66519318, 37.65014648), 1, 23, 23, 23, 23, 23, "28/07/2012")); //Rusia Europeana
        weatherReadings.add(new WeatherReading(5, "Uk                    ", new CoordinateMapper.Coordinate(51.48138290, -0.09887695), 1, 20, 23, 23, 23, 23, "28/07/2012")); //UK
        weatherReadings.add(new WeatherReading(5, "Ucraina               ", new CoordinateMapper.Coordinate(50.40151532, 30.56396484), 1, 26, 23, 23, 23, 23, "28/07/2012")); //Ucraina
        weatherReadings.add(new WeatherReading(5, "Danemarca             ", new CoordinateMapper.Coordinate(55.68068160, 12.57659912), 1, 18, 23, 23, 23, 23, "28/07/2012")); //Danemarca
        weatherReadings.add(new WeatherReading(5, "Finlanda              ", new CoordinateMapper.Coordinate(60.13056362, 24.93896484), 1, 17, 23, 23, 23, 23, "28/07/2012")); //Finlanda
        weatherReadings.add(new WeatherReading(5, "Polonia               ", new CoordinateMapper.Coordinate(52.10650519, 20.98388672), 1, 30, 23, 23, 23, 23, "28/07/2012")); //Polonia
        weatherReadings.add(new WeatherReading(5, "Ungaria               ", new CoordinateMapper.Coordinate(47.45780853, 19.08325195), 1, 25, 23, 23, 23, 23, "28/07/2012")); //Ungaria
        weatherReadings.add(new WeatherReading(5, "Turcia                ", new CoordinateMapper.Coordinate(41.02964339, 29.05883789), 1, 36, 23, 23, 23, 23, "28/07/2012")); //Turcia
        weatherReadings.add(new WeatherReading(5, "Grecia                ", new CoordinateMapper.Coordinate(37.94419750, 23.67553711), 1, 33, 23, 23, 23, 23, "28/07/2012")); //Grecia
        weatherReadings.add(new WeatherReading(5, "Italia                ", new CoordinateMapper.Coordinate(41.78769701, 12.57934570), 1, 31, 23, 23, 23, 23, "28/07/2012")); //Italia
        weatherReadings.add(new WeatherReading(5, "Franta                ", new CoordinateMapper.Coordinate(48.77791276, 2.449951170), 1, 27, 23, 23, 23, 23, "28/07/2012")); //Paris
        weatherReadings.add(new WeatherReading(5, "Germania              ", new CoordinateMapper.Coordinate(48.10743119, 11.61254883), 1, 22, 23, 23, 23, 23, "28/07/2012")); //Germania - muchen
        weatherReadings.add(new WeatherReading(5, "South                 ", new CoordinateMapper.Coordinate(29.26723287, 2.307128919), 1, 38, 23, 23, 23, 23, "28/07/2012")); //South

        return weatherReadings;
    }
}
