package com.miniearth.earthapp.WeatherLogicAndSync;

import com.miniearth.earthapp.Graphics2D.CoordinateMapper;
import com.miniearth.earthapp.Graphics2D.HeatMapFactory;

import java.io.Serializable;

/**
 * Created by kath on 7/5/15.
 */
public class WeatherReading implements Serializable, Comparable<WeatherReading> {
    int id;
    String name;
    Double latitude;
    Double longitude;
    int locationId;
    int temperature;
    int pressure;
    int windSpeed;
    int humidity;
    int precipitationChance;
    String time;
    float normalisedDisplayReading;

    public float getNormalisedDisplayReading() {
        return normalisedDisplayReading;
    }

    public void setNormalisedDisplayReading(float normalisedDisplayReading) {
        this.normalisedDisplayReading = normalisedDisplayReading;
    }

    static int WEATHER_READING_TYPE;

    public static void setReadingType(int readingType) {
        WEATHER_READING_TYPE = readingType;
    }

    public int getDisplayedField(){
        switch (WEATHER_READING_TYPE) {
            case HeatMapFactory.HUMIDITY:
                return this.getHumidity();
            case HeatMapFactory.PRECIPITATION_CHANCE:
                return this.getPrecipitationChance();
            case HeatMapFactory.PRESSURE:
                return this.getPressure();
            case HeatMapFactory.TEMPERATURE:
                return this.getTemperature();
            case HeatMapFactory.WINDSPEED:
                return this.getWindSpeed();
        }
        return 0;
    }

    @Override
    public int compareTo(WeatherReading weatherReading) {
        switch (WEATHER_READING_TYPE) {
            case HeatMapFactory.HUMIDITY:
                return this.getHumidity() - weatherReading.getHumidity();
            case HeatMapFactory.PRECIPITATION_CHANCE:
                return this.getPrecipitationChance() - weatherReading.getPrecipitationChance();
            case HeatMapFactory.PRESSURE:
                return this.getPressure() - weatherReading.getPressure();
            case HeatMapFactory.TEMPERATURE:
                return this.getTemperature() - weatherReading.getTemperature();
            case HeatMapFactory.WINDSPEED:
                return this.getWindSpeed() - weatherReading.getWindSpeed();
        }
        return 0;
    }

    public WeatherReading(int id,
                          String name,
                          CoordinateMapper.Coordinate coordinate,
                          int locationId,
                          int temperature,
                          int pressure,
                          int windSpeed,
                          int humidity,
                          int precipitationChance,
                          String time) {
        this.id = id;
        this.name = name;
        this.latitude = coordinate.getLatitude();
        this.longitude = coordinate.getLongitude();
        this.locationId = locationId;
        this.temperature = temperature;
        this.pressure = pressure;
        this.windSpeed = windSpeed;
        this.humidity = humidity;
        this.precipitationChance = precipitationChance;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public int getLocationId() {
        return locationId;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getPressure() {
        return pressure;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getPrecipitationChance() {
        return precipitationChance;
    }


    public String getTime() {
        return time;
    }
}
