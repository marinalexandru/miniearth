<!DOCTYPE html>
<html>
<head>
    <title>Welcome to earth!</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>

<style>
input, button, submit { border:none; background: none;}
</style>

<body role="document" style="padding-top: 70px;">
<%
//allow access only if session exists
String user = null;
if(session.getAttribute("user") == null){
    response.sendRedirect("login.html");
}else user = (String) session.getAttribute("user");
String userName = null;
String sessionID = null;
Cookie[] cookies = request.getCookies();
if(cookies !=null){
for(Cookie cookie : cookies){
    if(cookie.getName().equals("user")) userName = cookie.getValue();
    if(cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
}
}
%>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
         <div class="navbar-header">
                    <a class="navbar-brand" href="#">Weather api</a>
                </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <form action="<%=response.encodeURL("/logout") %>" method="post">
                    <button class="navbar-brand" type="submit" value="Logout" >Logout</button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container theme-showcase" role="main">
    <div class="jumbotron">
        <div class="row">
            <div class="col-lg-12">



                <br/>
                <p style="text-align: center">This is the admin <%=userName %> console for MiniEarth Android app. Get it from <a
                            href="https://play.google.com/store/apps/details?id=com.miniearth.app">Google Play</a>.</p>
            </div>
        </div>
    </div>
</div>

</body>
</html>
