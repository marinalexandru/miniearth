package com.miniearth.backend.Models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kath on 6/21/2015.
 */
public class WeatherForcastModel implements Serializable {

    //default id
    long id;
    long date;
    int temperature;
    int pressure;
    int windSpeed;
    int humidity;
    int precipitationChance;

    public WeatherForcastModel (){
        //initialised the time
        this.date = new Date().getTime();
    }

    public long getId(){
        return this.id;
    }

    public long getDate() {
        return date;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getPressure() {
        return pressure;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getPrecipitationChance() {
        return precipitationChance;
    }
}
