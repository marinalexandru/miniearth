/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Servlet Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloWorld
*/

package com.miniearth.backend.UIServlets;

import com.miniearth.backend.Utils.ServerResponse;

import java.io.IOException;

import javax.servlet.http.*;


public class MainServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        ServerResponse response = new ServerResponse(resp);
        response.printMessage("Please use the form to POST to this url",true);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        String name = req.getParameter("name");
        ServerResponse response = new ServerResponse(resp);
        if (name == null || name.isEmpty()) {
            response.printMessage("Please enter a name",true);
            return;
        }
        response.printMessage("Hello "+name,false);
    }
}
